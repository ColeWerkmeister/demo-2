#include <conio.h> //getch
#include <iostream> //cin/cout

using namespace std;

int main()
{
	int input;
	cout << "Enter an integer: ";
	cin >> input;

	if (input % 2 == 0)
	{
		cout << input << " is even.\n";
	}
	else 
	{
		cout << input << " is odd.\n";
	}
	

	_getch();
	return 0;
}